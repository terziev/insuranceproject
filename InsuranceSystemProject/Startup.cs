﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InsuranceSystemProject.Startup))]
namespace InsuranceSystemProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
