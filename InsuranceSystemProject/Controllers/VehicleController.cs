﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsuranceSystemProject.Models;

namespace InsuranceSystemProject.Controllers
{
    public class VehicleController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: Vehicle/VehicleIndex
        public ActionResult VehicleIndex()
        {
            return View(db.Vehicles.ToList());
        }

        /*
        // GET: Vehicle/VehicleDetails/5
        public ActionResult VehicleDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }
        */

        // GET: Vehicle/VehicleCreate
        public ActionResult VehicleCreate()
        {
            return View();
        }

        // POST: Vehicle/VehicleCreate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VehicleCreate([Bind(Include = "Id,Type,PolicyAutoes,PolicyKaskoes")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                db.Vehicles.Add(vehicle);
                db.SaveChanges();
                DisplaySuccessMessage("Has append a Vehicle record");
                return RedirectToAction("VehicleIndex");
            }

            DisplayErrorMessage();
            return View(vehicle);
        }

        // GET: Vehicle/VehicleEdit/5
        public ActionResult VehicleEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        // POST: VehicleVehicle/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VehicleEdit([Bind(Include = "Id,Type,PolicyAutoes,PolicyKaskoes")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicle).State = EntityState.Modified;
                db.SaveChanges();
                DisplaySuccessMessage("Has update a Vehicle record");
                return RedirectToAction("VehicleIndex");
            }
            DisplayErrorMessage();
            return View(vehicle);
        }

        // GET: Vehicle/VehicleDelete/5
        public ActionResult VehicleDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        // POST: Vehicle/VehicleDelete/5
        [HttpPost, ActionName("VehicleDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult VehicleDeleteConfirmed(int id)
        {
            Vehicle vehicle = db.Vehicles.Find(id);
            db.Vehicles.Remove(vehicle);
            db.SaveChanges();
            DisplaySuccessMessage("Has delete a Vehicle record");
            return RedirectToAction("VehicleIndex");
        }

        private void DisplaySuccessMessage(string msgText)
        {
            TempData["SuccessMessage"] = msgText;
        }

        private void DisplayErrorMessage()
        {
            TempData["ErrorMessage"] = "Save changes was unsuccessful.";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
