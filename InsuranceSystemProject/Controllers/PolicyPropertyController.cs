﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsuranceSystemProject.Models;

namespace InsuranceSystemProject.Controllers
{
    public class PolicyPropertyController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: PolicyProperty/PolicyPropertyIndex
        public ActionResult PolicyPropertyIndex(string searchby, string search)
        {
            var policyProperty = db.PolicyProperties.Include(p => p.AspNetUser).Include(p => p.City1).Include(p => p.InsuranceCompany).Include(p => p.PersonDetail);
            if (searchby == "Id")
            {
                int id = Int32.Parse(search);
                return View(db.PolicyProperties.Where(x => x.Id == id).ToList());
            }
            else if (searchby == "Client")
            {

                return View(db.PolicyProperties.Where(x => x.PersonDetail.FirstName == search).ToList());
            }
            else if (searchby == "Insurer")
            {

                return View(db.PolicyProperties.Where(x => x.UserId == search).ToList());
            }
            else
            {
                return View(policyProperty.ToList());
            }
        }

        /*
        // GET: PolicyProperty/PolicyPropertyDetails/5
        public ActionResult PolicyPropertyDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyProperty policyProperty = db.PolicyProperties.Find(id);
            if (policyProperty == null)
            {
                return HttpNotFound();
            }
            return View(policyProperty);
        }
        */

        // GET: PolicyProperty/PolicyPropertyCreate
        public ActionResult PolicyPropertyCreate()
        {
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName");
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name");
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName");
            return View();
        }

        // POST: PolicyProperty/PolicyPropertyCreate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PolicyPropertyCreate([Bind(Include = "Id,InsuranceValue,City,PolicyStartDate,UserId,PersonId,CompanyId,AspNetUser,City1,InsuranceCompany,PersonDetail")] PolicyProperty policyProperty)
        {
            if (ModelState.IsValid)
            {
                db.PolicyProperties.Add(policyProperty);
                db.SaveChanges();
                DisplaySuccessMessage("Has append a PolicyProperty record");
                return RedirectToAction("PolicyPropertyIndex");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", policyProperty.UserId);
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName", policyProperty.City);
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name", policyProperty.CompanyId);
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName", policyProperty.PersonId);
            DisplayErrorMessage();
            return View(policyProperty);
        }

        // GET: PolicyProperty/PolicyPropertyEdit/5
        public ActionResult PolicyPropertyEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyProperty policyProperty = db.PolicyProperties.Find(id);
            if (policyProperty == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", policyProperty.UserId);
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName", policyProperty.City);
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name", policyProperty.CompanyId);
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName", policyProperty.PersonId);
            return View(policyProperty);
        }

        // POST: PolicyPropertyPolicyProperty/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PolicyPropertyEdit([Bind(Include = "Id,InsuranceValue,City,PolicyStartDate,UserId,PersonId,CompanyId,AspNetUser,City1,InsuranceCompany,PersonDetail")] PolicyProperty policyProperty)
        {
            if (ModelState.IsValid)
            {
                db.Entry(policyProperty).State = EntityState.Modified;
                db.SaveChanges();
                DisplaySuccessMessage("Has update a PolicyProperty record");
                return RedirectToAction("PolicyPropertyIndex");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", policyProperty.UserId);
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName", policyProperty.City);
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name", policyProperty.CompanyId);
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName", policyProperty.PersonId);
            DisplayErrorMessage();
            return View(policyProperty);
        }

        // GET: PolicyProperty/PolicyPropertyDelete/5
        public ActionResult PolicyPropertyDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyProperty policyProperty = db.PolicyProperties.Find(id);
            if (policyProperty == null)
            {
                return HttpNotFound();
            }
            return View(policyProperty);
        }

        // POST: PolicyProperty/PolicyPropertyDelete/5
        [HttpPost, ActionName("PolicyPropertyDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult PolicyPropertyDeleteConfirmed(int id)
        {
            PolicyProperty policyProperty = db.PolicyProperties.Find(id);
            db.PolicyProperties.Remove(policyProperty);
            db.SaveChanges();
            DisplaySuccessMessage("Has delete a PolicyProperty record");
            return RedirectToAction("PolicyPropertyIndex");
        }

        private void DisplaySuccessMessage(string msgText)
        {
            TempData["SuccessMessage"] = msgText;
        }

        private void DisplayErrorMessage()
        {
            TempData["ErrorMessage"] = "Save changes was unsuccessful.";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
