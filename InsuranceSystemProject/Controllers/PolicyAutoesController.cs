﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsuranceSystemProject.Models;

namespace InsuranceSystemProject.Controllers
{
    public class PolicyAutoesController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: PolicyAutoes
        public ActionResult Index(string searchby, string search)
        {
            var policyAutoes = db.PolicyAutoes.Include(p => p.AspNetUser).Include(p => p.City1).Include(p => p.InsuranceCompany).Include(p => p.Make).Include(p => p.PersonDetail).Include(p => p.Usage).Include(p => p.Vehicle);



            if (searchby == "Id")
            {
                int id = Int32.Parse(search);
                return View(db.PolicyAutoes.Where(x => x.Id == id).ToList());
            }
            else if (searchby == "Client")
            {

                return View(db.PolicyAutoes.Where(x => x.PersonDetail.FirstName == search).ToList());
            }
            else if (searchby == "Insurer")
            {

                return View(db.PolicyAutoes.Where(x => x.UserId == search).ToList());
            }
            else
            {
                return View(policyAutoes.ToList());
            }
        }

        // GET: PolicyAutoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyAuto policyAuto = db.PolicyAutoes.Find(id);
            if (policyAuto == null)
            {
                return HttpNotFound();
            }
            return View(policyAuto);
        }

        // GET: PolicyAutoes/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName");
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name");
            ViewBag.MakeId = new SelectList(db.Makes, "Id", "Code");
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName");
            ViewBag.UsageId = new SelectList(db.Usages, "Id", "Title");
            ViewBag.VehicleId = new SelectList(db.Vehicles, "Id", "Type");
            return View();
        }

        // POST: PolicyAutoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MakeId,Model,RegNumber,RamNumber,PolicyStartDate,VehicleId,EnginePower,FirstRegistration,OwnerAge,YearsExperience,UsageId,City,GreenCard,LongAbroud,UserId,PersonId,CompanyId")] PolicyAuto policyAuto, [Bind(Include = "FirstName, MiddleName, LastName, EGN, CityId, Street, Number, Floor, Apartment, AdditionalInfo, Email, Mobile")] PersonDetail personDetail)
        {
            if (ModelState.IsValid)
            {
                db.PolicyAutoes.Add(policyAuto);
                db.PersonDetails.Add(personDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", policyAuto.UserId);
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName", policyAuto.City);
            ViewBag.CityId = new SelectList(db.Cities, "Id", "CityName", personDetail.City);
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name", policyAuto.CompanyId);
            ViewBag.MakeId = new SelectList(db.Makes, "Id", "Code", policyAuto.MakeId);
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName", policyAuto.PersonId);
            ViewBag.UsageId = new SelectList(db.Usages, "Id", "Title", policyAuto.UsageId);
            ViewBag.VehicleId = new SelectList(db.Vehicles, "Id", "Type", policyAuto.VehicleId);
            return View(policyAuto);
        }

        // GET: PolicyAutoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyAuto policyAuto = db.PolicyAutoes.Find(id);
            if (policyAuto == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", policyAuto.UserId);
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName", policyAuto.City);
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name", policyAuto.CompanyId);
            ViewBag.MakeId = new SelectList(db.Makes, "Id", "Code", policyAuto.MakeId);
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName", policyAuto.PersonId);
            ViewBag.UsageId = new SelectList(db.Usages, "Id", "Title", policyAuto.UsageId);
            ViewBag.VehicleId = new SelectList(db.Vehicles, "Id", "Type", policyAuto.VehicleId);
            return View(policyAuto);
        }

        // POST: PolicyAutoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MakeId,Model,RegNumber,RamNumber,PolicyStartDate,VehicleId,EnginePower,FirstRegistration,OwnerAge,YearsExperience,UsageId,City,GreenCard,LongAbroud,UserId,PersonId,CompanyId")] PolicyAuto policyAuto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(policyAuto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", policyAuto.UserId);
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName", policyAuto.City);
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name", policyAuto.CompanyId);
            ViewBag.MakeId = new SelectList(db.Makes, "Id", "Code", policyAuto.MakeId);
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName", policyAuto.PersonId);
            ViewBag.UsageId = new SelectList(db.Usages, "Id", "Title", policyAuto.UsageId);
            ViewBag.VehicleId = new SelectList(db.Vehicles, "Id", "Type", policyAuto.VehicleId);
            return View(policyAuto);
        }

        // GET: PolicyAutoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyAuto policyAuto = db.PolicyAutoes.Find(id);
            if (policyAuto == null)
            {
                return HttpNotFound();
            }
            return View(policyAuto);
        }

        // POST: PolicyAutoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PolicyAuto policyAuto = db.PolicyAutoes.Find(id);
            db.PolicyAutoes.Remove(policyAuto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
