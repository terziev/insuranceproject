﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsuranceSystemProject.Models;

namespace InsuranceSystemProject.Controllers
{
    public class MakeController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: Make/MakeIndex
        public ActionResult MakeIndex()
        {
            return View(db.Makes.ToList());
        }

        /*
        // GET: Make/MakeDetails/5
        public ActionResult MakeDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Make make = db.Makes.Find(id);
            if (make == null)
            {
                return HttpNotFound();
            }
            return View(make);
        }
        */

        // GET: Make/MakeCreate
        public ActionResult MakeCreate()
        {
            return View();
        }

        // POST: Make/MakeCreate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MakeCreate([Bind(Include = "Id,Code,Title,PolicyAutoes,PolicyKaskoes")] Make make)
        {
            if (ModelState.IsValid)
            {
                db.Makes.Add(make);
                db.SaveChanges();
                DisplaySuccessMessage("Has append a Make record");
                return RedirectToAction("MakeIndex");
            }

            DisplayErrorMessage();
            return View(make);
        }

        // GET: Make/MakeEdit/5
        public ActionResult MakeEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Make make = db.Makes.Find(id);
            if (make == null)
            {
                return HttpNotFound();
            }
            return View(make);
        }

        // POST: MakeMake/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MakeEdit([Bind(Include = "Id,Code,Title,PolicyAutoes,PolicyKaskoes")] Make make)
        {
            if (ModelState.IsValid)
            {
                db.Entry(make).State = EntityState.Modified;
                db.SaveChanges();
                DisplaySuccessMessage("Has update a Make record");
                return RedirectToAction("MakeIndex");
            }
            DisplayErrorMessage();
            return View(make);
        }

        // GET: Make/MakeDelete/5
        public ActionResult MakeDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Make make = db.Makes.Find(id);
            if (make == null)
            {
                return HttpNotFound();
            }
            return View(make);
        }

        // POST: Make/MakeDelete/5
        [HttpPost, ActionName("MakeDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult MakeDeleteConfirmed(int id)
        {
            Make make = db.Makes.Find(id);
            db.Makes.Remove(make);
            db.SaveChanges();
            DisplaySuccessMessage("Has delete a Make record");
            return RedirectToAction("MakeIndex");
        }

        private void DisplaySuccessMessage(string msgText)
        {
            TempData["SuccessMessage"] = msgText;
        }

        private void DisplayErrorMessage()
        {
            TempData["ErrorMessage"] = "Save changes was unsuccessful.";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
