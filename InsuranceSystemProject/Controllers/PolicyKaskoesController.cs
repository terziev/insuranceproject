﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsuranceSystemProject.Models;

namespace InsuranceSystemProject.Controllers
{
    public class PolicyKaskoesController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: PolicyKaskoes
        public ActionResult Index(string searchby, string search)
        {
            var policyKaskoes = db.PolicyKaskoes.Include(p => p.AspNetUser).Include(p => p.City1).Include(p => p.InsuranceCompany).Include(p => p.Make).Include(p => p.PersonDetail).Include(p => p.Vehicle);



            if (searchby == "Id")
            {
                int id = Int32.Parse(search);
                return View(db.PolicyKaskoes.Where(x => x.Id == id).ToList());
            }
            else if (searchby == "Client")
            {

                return View(db.PolicyKaskoes.Where(x => x.PersonDetail.FirstName == search).ToList());
            }
            else if (searchby == "Insurer")
            {

                return View(db.PolicyKaskoes.Where(x => x.UserId == search).ToList());
            }
            else
            {
                return View(policyKaskoes.ToList());
            }
        }

        // GET: PolicyKaskoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyKasko policyKasko = db.PolicyKaskoes.Find(id);
            if (policyKasko == null)
            {
                return HttpNotFound();
            }
            return View(policyKasko);
        }

        // GET: PolicyKaskoes/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName");
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name");
            ViewBag.MakeId = new SelectList(db.Makes, "Id", "Code");
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName");
            ViewBag.VehicleId = new SelectList(db.Vehicles, "Id", "Type");
            return View();
        }

        // POST: PolicyKaskoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,VehicleId,MakeId,InsuranceValue,Model,RegNumber,RamNumber,PolicyStartDate,EnginePower,FirstRegistration,City,UserId,PersonId,CompanyId")] PolicyKasko policyKasko)
        {
            if (ModelState.IsValid)
            {
                db.PolicyKaskoes.Add(policyKasko);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", policyKasko.UserId);
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName", policyKasko.City);
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name", policyKasko.CompanyId);
            ViewBag.MakeId = new SelectList(db.Makes, "Id", "Code", policyKasko.MakeId);
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName", policyKasko.PersonId);
            ViewBag.VehicleId = new SelectList(db.Vehicles, "Id", "Type", policyKasko.VehicleId);
            return View(policyKasko);
        }

        // GET: PolicyKaskoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyKasko policyKasko = db.PolicyKaskoes.Find(id);
            if (policyKasko == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", policyKasko.UserId);
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName", policyKasko.City);
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name", policyKasko.CompanyId);
            ViewBag.MakeId = new SelectList(db.Makes, "Id", "Code", policyKasko.MakeId);
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName", policyKasko.PersonId);
            ViewBag.VehicleId = new SelectList(db.Vehicles, "Id", "Type", policyKasko.VehicleId);
            return View(policyKasko);
        }

        // POST: PolicyKaskoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,VehicleId,MakeId,InsuranceValue,Model,RegNumber,RamNumber,PolicyStartDate,EnginePower,FirstRegistration,City,UserId,PersonId,CompanyId")] PolicyKasko policyKasko)
        {
            if (ModelState.IsValid)
            {
                db.Entry(policyKasko).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", policyKasko.UserId);
            ViewBag.City = new SelectList(db.Cities, "Id", "CityName", policyKasko.City);
            ViewBag.CompanyId = new SelectList(db.InsuranceCompanies, "Id", "Name", policyKasko.CompanyId);
            ViewBag.MakeId = new SelectList(db.Makes, "Id", "Code", policyKasko.MakeId);
            ViewBag.PersonId = new SelectList(db.PersonDetails, "Id", "FirstName", policyKasko.PersonId);
            ViewBag.VehicleId = new SelectList(db.Vehicles, "Id", "Type", policyKasko.VehicleId);
            return View(policyKasko);
        }

        // GET: PolicyKaskoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyKasko policyKasko = db.PolicyKaskoes.Find(id);
            if (policyKasko == null)
            {
                return HttpNotFound();
            }
            return View(policyKasko);
        }

        // POST: PolicyKaskoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PolicyKasko policyKasko = db.PolicyKaskoes.Find(id);
            db.PolicyKaskoes.Remove(policyKasko);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
