﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsuranceSystemProject.Models;

namespace InsuranceSystemProject.Controllers
{
    public class CityController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: City/CityIndex
        public ActionResult CityIndex()
        {
            return View(db.Cities.ToList());
        }

        /*
        // GET: City/CityDetails/5
        public ActionResult CityDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }
        */

        // GET: City/CityCreate
        public ActionResult CityCreate()
        {
            return View();
        }

        // POST: City/CityCreate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CityCreate([Bind(Include = "Id,CityName,PostCode,PersonDetails,PolicyAutoes,PolicyKaskoes,PolicyProperties")] City city)
        {
            if (ModelState.IsValid)
            {
                db.Cities.Add(city);
                db.SaveChanges();
                DisplaySuccessMessage("Has append a City record");
                return RedirectToAction("CityIndex");
            }

            DisplayErrorMessage();
            return View(city);
        }

        // GET: City/CityEdit/5
        public ActionResult CityEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        // POST: CityCity/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CityEdit([Bind(Include = "Id,CityName,PostCode,PersonDetails,PolicyAutoes,PolicyKaskoes,PolicyProperties")] City city)
        {
            if (ModelState.IsValid)
            {
                db.Entry(city).State = EntityState.Modified;
                db.SaveChanges();
                DisplaySuccessMessage("Has update a City record");
                return RedirectToAction("CityIndex");
            }
            DisplayErrorMessage();
            return View(city);
        }

        // GET: City/CityDelete/5
        public ActionResult CityDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        // POST: City/CityDelete/5
        [HttpPost, ActionName("CityDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult CityDeleteConfirmed(int id)
        {
            City city = db.Cities.Find(id);
            db.Cities.Remove(city);
            db.SaveChanges();
            DisplaySuccessMessage("Has delete a City record");
            return RedirectToAction("CityIndex");
        }

        private void DisplaySuccessMessage(string msgText)
        {
            TempData["SuccessMessage"] = msgText;
        }

        private void DisplayErrorMessage()
        {
            TempData["ErrorMessage"] = "Save changes was unsuccessful.";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
