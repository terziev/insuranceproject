﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsuranceSystemProject.Models;

namespace InsuranceSystemProject.Controllers
{
    public class InsuranceCompanyController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: InsuranceCompany/InsuranceCompanyIndex
        public ActionResult InsuranceCompanyIndex()
        {
            return View(db.InsuranceCompanies.ToList());
        }

        /*
        // GET: InsuranceCompany/InsuranceCompanyDetails/5
        public ActionResult InsuranceCompanyDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InsuranceCompany insuranceCompany = db.InsuranceCompanies.Find(id);
            if (insuranceCompany == null)
            {
                return HttpNotFound();
            }
            return View(insuranceCompany);
        }
        */

        // GET: InsuranceCompany/InsuranceCompanyCreate
        public ActionResult InsuranceCompanyCreate()
        {
            return View();
        }

        // POST: InsuranceCompany/InsuranceCompanyCreate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsuranceCompanyCreate([Bind(Include = "Id,Name,GoKoef,TravelKoef,AccidentKoef,PropertyKoef,KaskoKoef,LifeKoef,ResponsibilityKoef,PolicyAutoes,PolicyKaskoes,PolicyProperties")] InsuranceCompany insuranceCompany)
        {
            if (ModelState.IsValid)
            {
                db.InsuranceCompanies.Add(insuranceCompany);
                db.SaveChanges();
                DisplaySuccessMessage("Has append a InsuranceCompany record");
                return RedirectToAction("InsuranceCompanyIndex");
            }

            DisplayErrorMessage();
            return View(insuranceCompany);
        }

        // GET: InsuranceCompany/InsuranceCompanyEdit/5
        public ActionResult InsuranceCompanyEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InsuranceCompany insuranceCompany = db.InsuranceCompanies.Find(id);
            if (insuranceCompany == null)
            {
                return HttpNotFound();
            }
            return View(insuranceCompany);
        }

        // POST: InsuranceCompanyInsuranceCompany/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsuranceCompanyEdit([Bind(Include = "Id,Name,GoKoef,TravelKoef,AccidentKoef,PropertyKoef,KaskoKoef,LifeKoef,ResponsibilityKoef,PolicyAutoes,PolicyKaskoes,PolicyProperties")] InsuranceCompany insuranceCompany)
        {
            if (ModelState.IsValid)
            {
                db.Entry(insuranceCompany).State = EntityState.Modified;
                db.SaveChanges();
                DisplaySuccessMessage("Has update a InsuranceCompany record");
                return RedirectToAction("InsuranceCompanyIndex");
            }
            DisplayErrorMessage();
            return View(insuranceCompany);
        }

        // GET: InsuranceCompany/InsuranceCompanyDelete/5
        public ActionResult InsuranceCompanyDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InsuranceCompany insuranceCompany = db.InsuranceCompanies.Find(id);
            if (insuranceCompany == null)
            {
                return HttpNotFound();
            }
            return View(insuranceCompany);
        }

        // POST: InsuranceCompany/InsuranceCompanyDelete/5
        [HttpPost, ActionName("InsuranceCompanyDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult InsuranceCompanyDeleteConfirmed(int id)
        {
            InsuranceCompany insuranceCompany = db.InsuranceCompanies.Find(id);
            db.InsuranceCompanies.Remove(insuranceCompany);
            db.SaveChanges();
            DisplaySuccessMessage("Has delete a InsuranceCompany record");
            return RedirectToAction("InsuranceCompanyIndex");
        }

        private void DisplaySuccessMessage(string msgText)
        {
            TempData["SuccessMessage"] = msgText;
        }

        private void DisplayErrorMessage()
        {
            TempData["ErrorMessage"] = "Save changes was unsuccessful.";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
