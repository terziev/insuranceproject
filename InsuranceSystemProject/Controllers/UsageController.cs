﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsuranceSystemProject.Models;

namespace InsuranceSystemProject.Controllers
{
    public class UsageController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: Usage/UsageIndex
        public ActionResult UsageIndex()
        {
            return View(db.Usages.ToList());
        }

        /*
        // GET: Usage/UsageDetails/5
        public ActionResult UsageDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usage usage = db.Usages.Find(id);
            if (usage == null)
            {
                return HttpNotFound();
            }
            return View(usage);
        }
        */

        // GET: Usage/UsageCreate
        public ActionResult UsageCreate()
        {
            return View();
        }

        // POST: Usage/UsageCreate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UsageCreate([Bind(Include = "Id,Title,PolicyAutoes")] Usage usage)
        {
            if (ModelState.IsValid)
            {
                db.Usages.Add(usage);
                db.SaveChanges();
                DisplaySuccessMessage("Has append a Usage record");
                return RedirectToAction("UsageIndex");
            }

            DisplayErrorMessage();
            return View(usage);
        }

        // GET: Usage/UsageEdit/5
        public ActionResult UsageEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usage usage = db.Usages.Find(id);
            if (usage == null)
            {
                return HttpNotFound();
            }
            return View(usage);
        }

        // POST: UsageUsage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UsageEdit([Bind(Include = "Id,Title,PolicyAutoes")] Usage usage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usage).State = EntityState.Modified;
                db.SaveChanges();
                DisplaySuccessMessage("Has update a Usage record");
                return RedirectToAction("UsageIndex");
            }
            DisplayErrorMessage();
            return View(usage);
        }

        // GET: Usage/UsageDelete/5
        public ActionResult UsageDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usage usage = db.Usages.Find(id);
            if (usage == null)
            {
                return HttpNotFound();
            }
            return View(usage);
        }

        // POST: Usage/UsageDelete/5
        [HttpPost, ActionName("UsageDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult UsageDeleteConfirmed(int id)
        {
            Usage usage = db.Usages.Find(id);
            db.Usages.Remove(usage);
            db.SaveChanges();
            DisplaySuccessMessage("Has delete a Usage record");
            return RedirectToAction("UsageIndex");
        }

        private void DisplaySuccessMessage(string msgText)
        {
            TempData["SuccessMessage"] = msgText;
        }

        private void DisplayErrorMessage()
        {
            TempData["ErrorMessage"] = "Save changes was unsuccessful.";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
