﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsuranceSystemProject.Models
{
    [MetadataType(typeof(VehicleMetadata))]
    public partial class Vehicle
    {
    }

    public partial class VehicleMetadata
    {
        [Required(ErrorMessage = "Please enter : Id")]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Type")]
        [MaxLength(50)]
        public string Type { get; set; }

        [Display(Name = "PolicyAutoes")]
        public PolicyAuto PolicyAutoes { get; set; }

        [Display(Name = "PolicyKaskoes")]
        public PolicyKasko PolicyKaskoes { get; set; }

    }
}
