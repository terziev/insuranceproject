﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsuranceSystemProject.Models
{
    [MetadataType(typeof(InsuranceCompanyMetadata))]
    public partial class InsuranceCompany
    {
    }

    public partial class InsuranceCompanyMetadata
    {
        [Required(ErrorMessage = "Please enter : Id")]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter : Name")]
        [Display(Name = "Name")]
        [MaxLength(30)]
        public string Name { get; set; }

        [Display(Name = "GoKoef")]
        [Range(0, 10)]
        public decimal GoKoef { get; set; }

        [Display(Name = "TravelKoef")]
        [Range(0, 10)]
        public decimal TravelKoef { get; set; }

        [Display(Name = "AccidentKoef")]
        [Range(0, 10)]
        public decimal AccidentKoef { get; set; }

        [Display(Name = "PropertyKoef")]
        [Range(0, 10)]
        public decimal PropertyKoef { get; set; }

        [Display(Name = "KaskoKoef")]
        [Range(0, 10)]
        public decimal KaskoKoef { get; set; }

        [Display(Name = "LifeKoef")]
        [Range(0, 10)]
        public decimal LifeKoef { get; set; }

        [Display(Name = "ResponsibilityKoef")]
        [Range(0, 10)]
        public decimal ResponsibilityKoef { get; set; }

        [Required(ErrorMessage = "Please enter : PolicyAutoes")]
        [Display(Name = "PolicyAutoes")]
        public PolicyAuto PolicyAutoes { get; set; }

        [Required(ErrorMessage = "Please enter : PolicyKaskoes")]
        [Display(Name = "PolicyKaskoes")]
        public PolicyKasko PolicyKaskoes { get; set; }

        [Required(ErrorMessage = "Please enter : PolicyProperties")]
        [Display(Name = "PolicyProperties")]
        public PolicyProperty PolicyProperties { get; set; }

    }
}
