﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsuranceSystemProject.Models
{
    [MetadataType(typeof(UsageMetadata))]
    public partial class Usage
    {
    }

    public partial class UsageMetadata
    {
        [Required(ErrorMessage = "Please enter : Id")]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter : Title")]
        [Display(Name = "Title")]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter : PolicyAutoes")]
        [Display(Name = "PolicyAutoes")]
        public PolicyAuto PolicyAutoes { get; set; }

    }
}
