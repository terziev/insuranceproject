﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsuranceSystemProject.Models
{
    [MetadataType(typeof(CityMetadata))]
    public partial class City
    {
    }

    public partial class CityMetadata
    {
        [Required(ErrorMessage = "Please enter : Id")]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter : CityName")]
        [Display(Name = "CityName")]
        [MaxLength(255)]
        public string CityName { get; set; }

        [Required(ErrorMessage = "Please enter : PostCode")]
        [Display(Name = "PostCode")]
        [MaxLength(11)]
        public string PostCode { get; set; }

        [Required(ErrorMessage = "Please enter : PersonDetails")]
        [Display(Name = "PersonDetails")]
        public PersonDetail PersonDetails { get; set; }

        [Required(ErrorMessage = "Please enter : PolicyAutoes")]
        [Display(Name = "PolicyAutoes")]
        public PolicyAuto PolicyAutoes { get; set; }

        [Required(ErrorMessage = "Please enter : PolicyKaskoes")]
        [Display(Name = "PolicyKaskoes")]
        public PolicyKasko PolicyKaskoes { get; set; }

        [Required(ErrorMessage = "Please enter : PolicyProperties")]
        [Display(Name = "PolicyProperties")]
        public PolicyProperty PolicyProperties { get; set; }

    }
}
