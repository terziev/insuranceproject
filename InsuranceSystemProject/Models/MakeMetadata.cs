﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsuranceSystemProject.Models
{
    [MetadataType(typeof(MakeMetadata))]
    public partial class Make
    {
    }

    public partial class MakeMetadata
    {
        [Required(ErrorMessage = "Please enter : Id")]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter : Code")]
        [Display(Name = "Code")]
        [MaxLength(55)]
        public string Code { get; set; }

        [Required(ErrorMessage = "Please enter : Title")]
        [Display(Name = "Title")]
        [MaxLength(55)]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter : PolicyAutoes")]
        [Display(Name = "PolicyAutoes")]
        public PolicyAuto PolicyAutoes { get; set; }

        [Required(ErrorMessage = "Please enter : PolicyKaskoes")]
        [Display(Name = "PolicyKaskoes")]
        public PolicyKasko PolicyKaskoes { get; set; }

    }
}
