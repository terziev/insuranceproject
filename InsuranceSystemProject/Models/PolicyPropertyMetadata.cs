﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsuranceSystemProject.Models
{
    [MetadataType(typeof(PolicyPropertyMetadata))]
    public partial class PolicyProperty
    {
    }

    public partial class PolicyPropertyMetadata
    {
        [Required(ErrorMessage = "Please enter : Id")]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter : InsuranceValue")]
        [Display(Name = "InsuranceValue")]
        public int InsuranceValue { get; set; }

        [Required(ErrorMessage = "Please enter : City")]
        [Display(Name = "City")]
        public int City { get; set; }

        [Required(ErrorMessage = "Please enter : PolicyStartDate")]
        [Display(Name = "PolicyStartDate")]
        public DateTime PolicyStartDate { get; set; }

        [Display(Name = "UserId")]
        [MaxLength(128)]
        public string UserId { get; set; }

        [Required(ErrorMessage = "Please enter : PersonId")]
        [Display(Name = "PersonId")]
        public int PersonId { get; set; }

        [Required(ErrorMessage = "Please enter : CompanyId")]
        [Display(Name = "CompanyId")]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = "Please enter : AspNetUser")]
        [Display(Name = "AspNetUser")]
        public AspNetUser AspNetUser { get; set; }

        [Required(ErrorMessage = "Please enter : City1")]
        [Display(Name = "City1")]
        public City City1 { get; set; }

        [Required(ErrorMessage = "Please enter : InsuranceCompany")]
        [Display(Name = "InsuranceCompany")]
        public InsuranceCompany InsuranceCompany { get; set; }

        [Required(ErrorMessage = "Please enter : PersonDetail")]
        [Display(Name = "PersonDetail")]
        public PersonDetail PersonDetail { get; set; }

    }
}
